Build

Build and annotate the agent image

oc new-build https://gitlab.com/rocrisp/temp-ansible-jenkins-agent.git

oc label is temp-ansible-jenkins-agent role=jenkins-slave

Run

Create your pipeline reference this correct agent with node

oc new-build https://gitlab.com/rocrisp/testpipeline.git

*****
** Don't need to do this step unless you want to rebuild agent
To remove agent:

oc tag -d temp-ansible-jenkins-agent:latest

oc delete bc temp-ansible-jenkins-agent

*** Test, run it locally

docker build -t myimage:latest .

docker run -i -t myimage:latest /bin/bash
